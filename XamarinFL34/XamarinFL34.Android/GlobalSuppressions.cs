﻿
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Стиль", "IDE0028:Упростите инициализацию коллекции", Justification = "<Ожидание>", Scope = "member", Target = "~M:XamarinFL34.MainPage.FL34_Check_ConsAPIKEY(XamarinFL34.ConsData)")]

