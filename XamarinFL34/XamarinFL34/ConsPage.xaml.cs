﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Android.Content;

namespace XamarinFL34
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ConsPage : ContentPage
	{
        protected ConsData m_ConsData = null;
        OneConsultant m_SelectedOneConsultant = null;

        public ConsPage (ConsData consData)
		{
			InitializeComponent ();

            this.m_ConsData = consData;

            lsConsItemsList.ItemSelected += OnTapGestureRecognizer_Cons_Tapped;
            //lsConsItemsList.ItemTapped += OnTapGestureRecognizer_Cons_Tapped;
        }

        private async void BtnSearchClicked(object sender, EventArgs e)
        {
            string sSearchText = textSearch.Text;

            if (sSearchText.Length < 4)
            {
                await DisplayAlert("Ошибка", "Введите 4 символа или более для поиска", "OK");
                return;
            }

            string sBtnText = "";

            try
            {
                sBtnText = btnSearch.Text;
                btnSearch.Text = "Обработка...";
                btnSearch.IsEnabled = false;

                // Clear binding Data 
                lsCatalogConsHistoryList.BindingContext = null;
                lbCatalogHistoryTitle.Text = "Баллы каталогов: (выберите запись в списке)";
                m_SelectedOneConsultant = null;

                ///await DisplayAlert("Сообщение", "ButtonConfirmedClicked", "OK"); // DEBUG !!!

                // WEBAPI:begin
                var url = "https://faberlic-doma.ru/v/api/NNNNN.php"; // Insert Your URL here

                var client = new HttpClient();
                List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
                var postData = list;

                // add & fill WEBAPI POST params
                postData.Add(new KeyValuePair<string, string>("cons_number", m_ConsData.ConsNumber));
                postData.Add(new KeyValuePair<string, string>("apikey", m_ConsData.APIKEY));
                postData.Add(new KeyValuePair<string, string>("search_text", sSearchText));

                // Call WebAPI
                HttpContent content = new FormUrlEncodedContent(postData);
                var response = await client.PostAsync(url, content);
                var result = await response.Content.ReadAsStringAsync();
                string jsonResult = result.ToString();

                // Show WebAPI response - DEBUG
                ///await DisplayAlert("Результат запроса", jsonResult, "OK");

                // get WebAPI response as JSON object
                JObject jsonObj = JObject.Parse(jsonResult);

                // get JSON "error" value
                string sJsonError = jsonObj["error"].ToString();

                ///string sMess;

                // check WEPAPI response
                if (sJsonError.Length > 0)
                { // ERROR!
                    m_ConsData.ErrorMessage = "ОШИБКА: " + sJsonError.Length;
                    ///await DisplayAlert("Ошибка", sMess, "OK");
                }
                else
                { // Ok, normal result
                    // take Data from WEBAPI
                    string sRowsCount = jsonObj["response"]["count"].ToString();
                    lbConsTitle.Text = "Кол-во: " + sRowsCount;

                    string sJson = jsonObj["response"]["rows"].ToString();
                    // ? при десериализации попадают ли "history" в объекты OneConsultant в items???
                    var items = JsonConvert.DeserializeObject<OneConsultant[]>(sJson);

                    // Binding Data to UI ListView control
                    lsConsItemsList.BindingContext = items;

                    //await DisplayAlert("Сообщение", "Данные получены: " + items, "OK");
                }
                // WEBAPI:end
            }
            catch (Exception ex)
            {
                m_ConsData.ErrorMessage = "ОШИБКА: " + ex.Message.ToString();
                await DisplayAlert("ОШИБКА", "ОШИБКА: " + ex.Message.ToString(), "OK");
            }
            finally
            {
                btnSearch.Text = sBtnText;
                btnSearch.IsEnabled = true;
            }
        }

        private async void BtnCopyFIOClicked(object sender, EventArgs e)
        {
            if (m_SelectedOneConsultant == null)
            {
                DisplayAlert("ОШИБКА", "ОШИБКА: выберите сначала запись в списке", "OK");
                return;
            }

            string copyText = m_SelectedOneConsultant._FullFIO;
            PasteTextToClipboard(copyText);
        }

        private async void BtnCopyPhoneClicked(object sender, EventArgs e)
        {
            if (m_SelectedOneConsultant == null)
            {
                DisplayAlert("ОШИБКА", "ОШИБКА: выберите сначала запись в списке", "OK");
                return;
            }

            string copyText = m_SelectedOneConsultant.Phone;
            PasteTextToClipboard(copyText);
        }

        private async void BtnCopyEMailClicked(object sender, EventArgs e)
        {
            if (m_SelectedOneConsultant == null)
            {
                DisplayAlert("ОШИБКА", "ОШИБКА: выберите сначала запись в списке", "OK");
                return;
            }

            string copyText = m_SelectedOneConsultant.EMail;

            if (!String.IsNullOrEmpty(copyText))
                PasteTextToClipboard(copyText);
            else
                DisplayAlert("Сообщение", "Пусто, нет данных", "OK");
        }

        private async void BtnCopyRefLinkClicked(object sender, EventArgs e)
        {
            if (m_SelectedOneConsultant == null)
            {
                DisplayAlert("ОШИБКА", "ОШИБКА: выберите сначала запись в списке", "OK");
                return;
            }

            string copyText = m_SelectedOneConsultant.RefLink;

            if (!String.IsNullOrEmpty(copyText))
                PasteTextToClipboard(copyText);
            else
                DisplayAlert("Сообщение", "Пусто, нет данных", "OK");
        }

        private async void PasteTextToClipboard(string srcText)
        {
            var clipboardManager = (ClipboardManager)Forms.Context.GetSystemService(Context.ClipboardService);
            Android.Content.ClipData clip = Android.Content.ClipData.NewPlainText("Android Clipboard", srcText);
            clipboardManager.PrimaryClip = clip;

            await DisplayAlert("Скопировано", "Скопирован в буфер обмена текст: " + srcText, "OK");
        }

        private async void OnTapGestureRecognizer_Cons_Tapped(object sender, EventArgs args)
        {
            //Label lb = sender as Label;
            //string s = lb.Text;
            //PasteTextToClipboard(s);

            OneConsultant oSel = lsConsItemsList.SelectedItem as OneConsultant;

            // remember selected Cons item
            m_SelectedOneConsultant = oSel;

            // Binding Data to UI ListView control
            lsCatalogConsHistoryList.BindingContext = oSel.listHistory;

            string sConsData = oSel.FullData;
            lbCatalogHistoryTitle.Text = "Баллы каталогов (ЛО, ОЛГ, ГО): " + oSel.Number;
            //await DisplayAlert("Выбрана запись", sConsData, "OK");
        }

    }
}