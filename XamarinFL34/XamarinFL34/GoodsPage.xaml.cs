﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Android.Content;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace XamarinFL34
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class GoodsPage : ContentPage
	{
        protected ConsData m_ConsData = null;

        /// <summary>
        /// Flag: is BUSY event handler 
        /// </summary>
        protected bool m_isBusy_OnTapGestureRecognizer_GoodsImage_Tapped = false;

        public GoodsPage (ConsData consData)
		{
			InitializeComponent ();

            // assign ConsData object
            m_ConsData = consData;

            GetData();
        }

        private void BtnGetList_Clicked(object sender, EventArgs e)
        {
            GetData();
        }

        /// <summary>
        /// Event: Goods item Image clicked. Use m_isBusy_OnTapGestureRecognizer_GoodsImage_Tapped for locking.
        /// </summary>
        /// <param name="sender">sender as Image</param>
        /// <param name="args"></param>
        async private void OnTapGestureRecognizer_GoodsImage_Tapped(object sender, EventArgs args)
        {
            if (m_isBusy_OnTapGestureRecognizer_GoodsImage_Tapped)
                return;

            // set Flag m_isBusy_OnTapGestureRecognizer_GoodsImage_Tapped
            m_isBusy_OnTapGestureRecognizer_GoodsImage_Tapped = true; 

            Image image = null;

            try
            {
                image = sender as Image;

                await image.ScaleTo(2, 2000);
                await image.RotateTo(360, 3000);
                await image.ScaleTo(1, 2000);
            }
            catch (Exception) {
                if (image != null)
                {
                    image.ScaleTo(1, 500);
                }
            }
            finally
            {
                // reset Flag m_isBusy_OnTapGestureRecognizer_GoodsImage_Tapped
                m_isBusy_OnTapGestureRecognizer_GoodsImage_Tapped = false;
            }
        }

        private async void GetData()
        {
            string sBtnText = "";

            try
            {
                sBtnText = BtnGetList.Text;
                BtnGetList.Text = "Обработка...";
                BtnGetList.IsEnabled = false;
                BtnGetList_Footer.Text = "Обработка...";
                BtnGetList_Footer.IsEnabled = false;

                // WEBAPI:begin
                var url = "https://faberlic-doma.ru/v/api/MMMMM.php"; // Insert Your URL here
                // test WEB API: https://resttesttest.com/

                var client = new HttpClient();
                List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
                var postData = list;

                // add & fill WEBAPI POST params
                string goods_count = "10";
                //postData.Add(new KeyValuePair<string, string>("cons_number", m_ConsData.ConsNumber));
                //postData.Add(new KeyValuePair<string, string>("apikey", m_ConsData.APIKEY));
                postData.Add(new KeyValuePair<string, string>("goods_count", goods_count));

                // Call WebAPI
                HttpContent content = new FormUrlEncodedContent(postData);
                var response = await client.PostAsync(url, content);
                var result = await response.Content.ReadAsStringAsync();
                string jsonResult = result.ToString();

                // Show WebAPI response - DEBUG
                ///await DisplayAlert("Результат запроса", jsonResult, "OK");

                // get WebAPI response as JSON object
                JObject jsonObj = JObject.Parse(jsonResult);

                // get JSON "error" value
                string sJsonError = jsonObj["error"].ToString();

                ///string sMess;

                // check WEPAPI response
                if (sJsonError.Length > 0)
                { // ERROR!
                    m_ConsData.ErrorMessage = "ОШИБКА: " + sJsonError.Length;
                    ///await DisplayAlert("Ошибка", sMess, "OK");
                }
                else
                { // Ok, normal result
                    // take Data from WEBAPI
                    string sRowsCount = jsonObj["response"]["count"].ToString();
                    lbTitle.Text = "Кол-во: " + sRowsCount;

                    string sJson = jsonObj["response"]["rows"].ToString();
                    var items = JsonConvert.DeserializeObject<OneGoodsItem[]>(sJson);

                    // Binding Data to UI ListView control
                    lsItemsList.BindingContext = items;

                    lsItemsList.SelectedItem = 0;
                    lsItemsList.Focus();

                    //await DisplayAlert("Сообщение", "Данные получены: " + items, "OK");
                }
                // WEBAPI:end

                // Visible footer Button
                BtnGetList_Footer.IsVisible = true;

            }
            catch (Exception ex)
            {
                m_ConsData.ErrorMessage = "ОШИБКА: " + ex.Message.ToString();
                await DisplayAlert("ОШИБКА", "ОШИБКА: " + ex.Message.ToString(), "OK");
            }
            finally
            {
                BtnGetList.Text = sBtnText;
                BtnGetList.IsEnabled = true;

                BtnGetList_Footer.Text = sBtnText;
                BtnGetList_Footer.IsEnabled = true;
            }

            //try
            //{
            //    OneGoodsItem[] list = new OneGoodsItem[2];
            //    list[0] = new OneGoodsItem
            //    {
            //        goods_name = "Тушь для ресниц «Пуш-ап эффект»",
            //        goods_sarticle = "5471",
            //        goods_imgurl = "https://images.faberlic.com/images/fl/TflGoods/md/1000253161989_15123969052.jpg",
            //        goods_price = "249"
            //    };

            //    list[1] = new OneGoodsItem
            //    {
            //        goods_name = "Комплект выносных массажных электродов «ДЭНАС-массажный»",
            //        goods_sarticle = "77644",
            //        goods_imgurl = "https://images.faberlic.com/images/fl/TflGoods/md/1000451940293_15133447431.jpg",
            //        goods_price = "3699"
            //    };

            //    lbTitle.Text = "Количество: " + "2";

            //    // Binding Data to UI ListView control
            //    lsItemsList.BindingContext = list;

            //    lsItemsList.SelectedItem = 0;
            //    lsItemsList.Focus();

            //    // Visible footer Button
            //    BtnGetList_Footer.IsVisible = true;
            //}
            //catch (Exception ex)
            //{
            //    await DisplayAlert("Ошибка", "Ошибка: " + ex.Message.ToString(), "OK");
            //}

        }

    }
}