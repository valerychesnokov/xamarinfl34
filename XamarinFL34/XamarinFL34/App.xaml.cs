﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace XamarinFL34
{
	public partial class App : Application
	{
		public App ()
		{
			InitializeComponent();

			///MainPage = new NavigationPage(new XamarinFL34.MainPage());
            MainPage = new FL34NavigationPage(new XamarinFL34.StartMasterDetailPage());

            //NavigationPage navPage = (NavigationPage)MainPage;
            //navPage.BarBackgroundColor = Color.FromHex("#ff6969");
            //navPage.BarTextColor = Color.FromHex("#ffffff");
        }

        protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
