﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using System.Net.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace XamarinFL34
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BirthDaysPage : ContentPage
	{
        protected ConsData m_ConsData = null;

        public BirthDaysPage (ConsData consData)
		{
			InitializeComponent ();

            // assign ConsData object
            m_ConsData = consData;

            GetData();
        }

        // get Data from WEB API
        async void GetData()
        {
            try
            {
                lbBirthDaysTitle.Text = "Обработка...";

                ///await DisplayAlert("Сообщение", "ButtonConfirmedClicked", "OK"); // DEBUG !!!

                // WEBAPI:begin
                var url = "https://faberlic-doma.ru/v/NNNNNNN.php";

                var client = new HttpClient();
                List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
                var postData = list;

                // add & fill WEBAPI POST params
                postData.Add(new KeyValuePair<string, string>("cons_number", m_ConsData.ConsNumber));
                postData.Add(new KeyValuePair<string, string>("apikey", m_ConsData.APIKEY));

                // Call WebAPI
                HttpContent content = new FormUrlEncodedContent(postData);
                var response = await client.PostAsync(url, content);
                var result = await response.Content.ReadAsStringAsync();
                string jsonResult = result.ToString();

                // Show WebAPI response - DEBUG
                ///await DisplayAlert("Результат запроса", jsonResult, "OK");

                // get WebAPI response as JSON object
                JObject jsonObj = JObject.Parse(jsonResult);

                // get JSON "error" value
                string sJsonError = jsonObj["error"].ToString();

                ///string sMess;

                // check WEPAPI response
                if (sJsonError.Length > 0)
                { // ERROR!
                    m_ConsData.ErrorMessage = "ОШИБКА: " + sJsonError.Length;
                    ///await DisplayAlert("Ошибка", sMess, "OK");
                }
                else
                { // Ok, normal result
                    // take Data from WEBAPI
                    string sRowsCount = jsonObj["response"]["count"].ToString();
                    lbBirthDaysTitle.Text = "🎁 Дни Рождения консультантов: " + sRowsCount;

                    string sJson = jsonObj["response"]["rows"].ToString();
                    var items = JsonConvert.DeserializeObject<OneBirthDays[]>(sJson);

                    // Binding Data to UI ListView control
                    lsBirthDaysList.BindingContext = items;

                    //await DisplayAlert("Сообщение", "Данные получены: " + items, "OK");
                }
                // WEBAPI:end
            }
            catch (Exception ex)
            {
                m_ConsData.ErrorMessage = "ОШИБКА: " + ex.Message.ToString();
                await DisplayAlert("ОШИБКА", "ОШИБКА: " + ex.Message.ToString(), "OK");
            }
            //finally
            //{
            //    btnSearch.Text = sBtnText;
            //    btnSearch.IsEnabled = true;
            //}
        }
    }
}